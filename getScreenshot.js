
const CDP = require('chrome-remote-interface');
const argv = require('minimist')(process.argv.slice(2));
const file = require('mz/fs');
const timeout = require('delay');


//設定が可能な項目
/********************************************************************/
//const url = argv.url || 'https://www.google.com';
//キャプチャするurl
const url = argv.url || 'http://localhost:8080/marketdigitalcurrencyfx/';

//0:表示しない
//0以外:表示
const consolelog=1;

//キャプチャする幅(数値は、引数指定しなかった場合の初期値)
const viewportWidth = argv.viewportWidth || 769;
//キャプチャする高さ(数値は、引数指定しなかった場合の初期値)
let viewportHeight = argv.viewportHeight || 360;
//ブラウザの表示のための待機時間。クルクルしたりしているので、表示されるまで待つ
const delay = argv.delay || 8000;
/********************************************************************/

// CLI Args
const format = argv.format === 'jpeg' ? 'jpeg' : 'png';
const userAgent = argv.userAgent;
const fullPage = argv.full;
const outputDir = argv.outputDir || './';
const output = argv.output || `output.${format === 'png' ? 'png' : 'jpg'}`;

init();

async function init() {
  let client;
  try {
    // Start the Chrome Debugging Protocol
    client = await CDP();

    // Verify version
    const { Browser } = await CDP.Version();
    const browserVersion = Browser.match(/\/(\d+)/)[1];
    if (Number(browserVersion) !== 60) {
      console.warn(`This script requires Chrome 60, however you are using version ${browserVersion}. The script is not guaranteed to work and you may need to modify it.`);
    }

    // Extract used DevTools domains.
    const {DOM, Emulation, Network, Page, Runtime} = client;

    // Enable events on domains we are interested in.
    await Page.enable();
    await DOM.enable();
    await Network.enable();

    // If user agent override was specified, pass to Network domain
    if (userAgent) {
      await Network.setUserAgentOverride({userAgent});
    }

    // Set up viewport resolution, etc.
    const deviceMetrics = {
      width: viewportWidth,
      height: viewportHeight,
      deviceScaleFactor: 0,
      mobile: false,
      fitWindow: false,
    };
    await Emulation.setDeviceMetricsOverride(deviceMetrics);
    await Emulation.setVisibleSize({
      width: viewportWidth,
      height: viewportHeight,
    });

    // Navigate to target page
    await Page.navigate({url});

    // Wait for page load event to take screenshot
    await Page.loadEventFired();

    await timeout(delay);

    // If the `full` CLI option was passed, we need to measure the height of
    // the rendered page and use Emulation.setVisibleSize
    if (fullPage) {
      const {root: {nodeId: documentNodeId}} = await DOM.getDocument();
      const {nodeId: bodyNodeId} = await DOM.querySelector({
        selector: 'body',
        nodeId: documentNodeId,
      });
      const {model} = await DOM.getBoxModel({nodeId: bodyNodeId});
      viewportHeight = model.height;

      await Emulation.setVisibleSize({width: viewportWidth, height: viewportHeight});
      // This forceViewport call ensures that content outside the viewport is
      // rendered, otherwise it shows up as grey. Possibly a bug?
      await Emulation.forceViewport({x: 0, y: 0, scale: 1});
    }
myConsoleLog(viewportWidth);
myConsoleLog(viewportHeight);
const screenshot = await Page.captureScreenshot({
      format,
      fromSurface: true,
      clip: {
        width: viewportWidth,
        height: viewportHeight
      }
    });
    const buffer = new Buffer(screenshot.data, 'base64');
    const path = `${outputDir + output}`;
    await file.writeFile(path, buffer, 'base64');
    console.log('Success Screenshot saved.['+path+']');
    client.close();
  } catch (err) {
    if (client) {
      client.close();
    }
    console.error('Exception while taking screenshot:', err);
    process.exit(1);
  }
}

function myConsoleLog(sz){
  if(consolelog !=0 ){
    console.log(sz);
  }
}

