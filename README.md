初期設定


コマンド
#ユーザ作成
sudo adduser ec2-user

#sudo グループに追加
sudo gpasswd -a ec2-user sudo


sudo cp /home/ubuntu/.ssh/authorized_keys /home/ec2-user/.ssh/
cd /home/ec2-user/.ssh/
sudo chown ec2-user *
sudo chgrp ec2-user *

sudo chown ec2-user /home/ec2-user/.ssh/
sudo chgrp ec2-user /home/ec2-user/.ssh/


ubuntuユーザー
$ sudo apt-get install optipng
#nodejsのバージョン8指定
curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
sudo apt-get install -y nodejs

chromeのインストール
https://www.slimjet.com/chrome/google-chrome-old-version.php
wget https://www.slimjet.com/chrome/download-chrome.php?file=lnx%2Fchrome64_60.0.3112.90.deb
#ファイル名を変更
mv "download-chrome.php?file=lnx%2Fchrome64_60.0.3112.90.deb" chrome64_60.0.3112.90.deb
#↓いるかも
sudo dpkg -i chrome64_60.0.3112.90.deb

>Errors were encountered while processing:
>google-chrome-stable
エラーが出た場合は、↓して、再インストール
sudo apt-get install -f

#インストールとバージョン確認
$ google-chrome -version
Google Chrome 60.0.3112.90 

#↓ここからec2-user必須。使用するライブラリを入れる
sudo su - ec2-user
パスワード:GMOcoin123
cd /home/ec2-user/autotwitter
npm i -S chrome-remote-interface
npm i -S minimist
npm i -S mz
npm i -S delay


#chromeの起動
sudo service google-chrome --headless --remote-debugging-port=9222 --disable-gpu start&

#chromeの起動監視shell
check_process.sh


#ブラウザでsh敵のurl開くjs
/usr/bin/nodejs /home/ec2-user/autotwitter/getScreenshot.js  --delay=8000 --viewportWidth=700 --viewportHeight=392 --url='https://coin.z.com/jp/corp/twchart/1002.html' --outputDir='/home/ec2-user/autotwitter/'

#画像の変換処理
#pngquantが一番、設定、使い方が優しかった
sudo apt-get update
sudo apt-get install pngquant

#アインストール確認
pngquant --version

#使い方
#256色のscreenshot.pngを作成する(生成ファイル名は、固定で****.png)
pngquant 256 screenshot.png

#一応、ubuntuでの場所
https://ubuntu.pkgs.org/14.04/ubuntu-universe-i386/pngquant_2.0.1-1_i386.deb.html
http://archive.ubuntu.com/ubuntu/pool/universe/p/pngquant/pngquant_2.0.1-1_i386.deb


/usr/bin/nodejs /home/ec2-user/autotwitter/getScreenshot.js  --delay=8000 --viewportWidth=700 --viewportHeight=392 --url='https://coin.z.com/jp/corp/twchart/1002.html' --outputDir='/home/ec2-user/autotwitter/'
mv /home/ec2-user/autotwitter/output.png /home/ec2-user/autotwitter/1002.png
pngquant 256 --ext -256.png --force /home/ec2-user/autotwitter/1002.png


#s3
#goofys

sudo vi ~/.profile
if [ -d "$HOME/go" ] ; then
    GOPATH="$HOME/go"
fi
if [ -d "$GOPATH/bin" ] ; then
    PATH="$PATH:$GOPATH/bin"
fi

PATH=$PATH:$GOPATH
export PATH
#重要:パスを通しているか?フルパスでコマンドを実施するか
#ubuntuで実施

wget https://dl.google.com/go/go1.9.4.linux-amd64.tar.gz
 $ sudo tar -C /usr/local -xzf go1.9.4.linux-amd64.tar.gz
 $ export PATH=$PATH:/usr/local/go/bin
 $ go version
 go version go1.8.3 linux/amd64
#環境によってはうまくインストーメル出来ない
#バージョン重要。apt-get等で、すでに古いバージョンが入っている場合は、sudo apt-get purge golangで削除する

$ [ ! -e ~/go/ ] && mkdir ~/go
$ export GOPATH=$HOME/go
$ export PATH="$PATH:$GOPATH/bin"
$ git --version
$ go get github.com/kahing/goofys
$ go install github.com/kahing/goofys
$ goofys -h
$ sudo apt-get install awscli
$ mkdir /mnt/mnt-goofys

https://s3.console.aws.amazon.com/s3/buckets/coin.z.com.common/?region=ap-northeast-1&tab=overview


$ /home/ubuntu/go/bin/goofys

#マウントする際は、EC2-USERで行う。アプリのユーザーでマウントしないと、権限で書き込みに失敗するみたい
#それっぽいエラーメッセージは「でない」ので、注意
$ sudo su - ec2-user
$ goofys staging.zcomwallet.com.common /mnt/mnt-goofys
$ sudo chown ec2-ser /mnt/mnt-goofys
$ sudo chgrp ec2-ser /mnt/mnt-goofys
$ /home/ubuntu/go/bin/goofys coin.z.com.common /mnt/mnt-goofys

#S3のストレージ一覧の出力コマンド
$ aws s3 ls

aws configure
staging-corp-developer
アクセスキー ID:AKIAJHOGVSEKWPFZLYWA
シークレットアクセスキー:giR/KkKTx2iBUnKmYYPCayATaD1zJkroQm0e5Jp3

GMOcoin123

s3
プロパティ
Static website hosting 
https://s3.console.aws.amazon.com/s3/buckets/staging.zcomwallet.com.common/?region=ap-northeast-1&tab=properties

このバケットを使用してウェブサイトをホストする
を有効にする


#EC2-USERのエリアをJSTにする
sudo timedatectl set-timezone Asia/Tokyo
↑しないと、チャート(https://coin.z.com/jp/corp/twchart/1002-4.html)の時間軸がエリア付の時刻になる。
具体的には、サーバ時刻をUTCに設定している場合、時間軸が-9時間されてしまう


その他、こまったときのコマンド
#ファイルげっと
scp -i /Users/t-toyama/Documents/aws/zcomwallet-stg/zcomwallet-dev-key-20170131.pem ec2-user@stg-corp-batch1e:/home/ec2-user/screenshot.png .

scp -F config ec2-user@stg-corp-batch1e:/home/ec2-user/screenshot.png .
scp -F config ec2-user@stg-corp-batch1e:/home/ec2-user/screenshot-fs8.png .
scp -F config ec2-user@stg-corp-batch1e:/home/ec2-user/autotwitter/output.png .
scp -F config ec2-user@stg-corp-batch1e:/home/ec2-user/autotwitter/1002-256.png .

scp -F ~/.ssh/config cookie.txt ec2-user@stg-corp-batch1e:/home/ec2-user/screenshot.png 

scp -F ~/.ssh/config ec2-user@stg-corp-batch1e:/home/ec2-user/autotwitter/*.sh ./
scp -F ~/.ssh/config ec2-user@stg-corp-batch1e:/home/ec2-user/autotwitter/*.js ./

scp -F ~/.ssh/config ./*.sh ec2-user@corp-batch1e:/home/ec2-user/autotwitter/
scp -F ~/.ssh/config ./*.js ec2-user@corp-batch1e:/home/ec2-user/autotwitter/



chart-imgs
https://staging.zcomwallet.com/chart-imgs/1002-4-256.png


https://coin.z.com/
https://coin.z.com/jp/corp/twchart/1002-4.html